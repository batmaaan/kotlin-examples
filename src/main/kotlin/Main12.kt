import abstracts.ATemplate
import abstracts.BaseTemplate
import interfases.A
import interfases.B
import interfases.MyInterface
import openexam.Driver
import openexam.Person

fun main(args: Array<String>) {
    val person = Person("Alex")
    val driver = Driver("Alex")

    println(person)
    println(driver)

    println(person.getAddress())
    println(driver.getAddress())
}




